import webbrowser
import time
#number of breaks needed is 3
breaks_needed = 3
# Initialize the counter to zero
breaks_count_now = 0

# Print when program started
print("This program started on" + time.ctime())
# Check count is lessthan breaks_needed i.e is 3
while(breaks_count_now < breaks_needed):
    #sleep for 20 sec 
    time.sleep(20)
    #Open the given link in the browser
    webbrowser.open("http://www.youtube.com")
    #Increment the breaks_count_now by 1
    breaks_count_now = breaks_count_now + 1
